# Project-Euler-in-Haskell
I'll be doing the problems over on https://projecteuler.net/ incrementally in my spare time. Project Euler is a programming challenge where you write programs to solve math problems that are given. For the most part they'll be done in haskell.
